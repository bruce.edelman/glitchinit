# Glitch Fitting Initialization Script

    git clone https://git.ligo.org/bruce.edelman/glitchinit.git

Here is the script run command with the options I have been using. The frame files are not in this repository as they are too large:

The Frame Files can be found the CIT cluster at:



    CIT:/hdfs/frames/ER8/hoft_C01/H1/H-H1_HOFT_C01-11260/H-H1_HOFT_C01-1126096896-4096.gwf
    CIT:/hdfs/frames/ER8/hoft_C01/H1/H-H1_HOFT_C01-11261/H-H1_HOFT_C01-1126100992-4096.gwf
    CIT:/hdfs/frames/ER8/hoft_C01/H1/H-H1_HOFT_C01-11261/H-H1_HOFT_C01-1126105088-4096.gwf
    CIT:/hdfs/frames/ER8/hoft_C01/L1/L-L1_HOFT_C01-11260/L-L1_HOFT_C01-1126096896-4096.gwf
    CIT:/hdfs/frames/ER8/hoft_C01/L1/L-L1_HOFT_C01-11261/L-L1_HOFT_C01-1126100992-4096.gwf
    CIT:/hdfs/frames/ER8/hoft_C01/L1/L-L1_HOFT_C01-11261/L-L1_HOFT_C01-1126105088-4096.gwf

## Example Run

    /usr/bin/python3 InitGlitch.py --H1_data_file H-H1_HOFT_C01-112609686-4096.gwf H-H1_HOFT_C01-1126100992-4096.gwf H-H1_HOFT_C01-1126105088-4096.gwf --L1_data_file L-L1_HOFT_C01-112609686-4096.gwf L-L1_HOFT_C01-1126100992-4096.gwf L-L1_HOFT_C01-1126105088-4096.gwf --H1_channel H1:DCS-CALIB_STRAIN_C01 --L1_channel L1:DCS-CALIB_STRAIN_C01 --trigtime 1126103127.521729 --seglen 8 --srate 1024--psds 1126103127.522_H1-PSD.dat 1126103127.522_L1-PSD.dat --nglitch 5 --debug

## Extra Options

For full list of options and helpful descriptive text run:
    
    python InitGlitch.py --help

TO CHECK GLITCH  SIGNAL SUBTRACTION ON THIS DATA ADD THESE OPTIONS:

to inject signals to noise data:

    --H1_inject injection_H1.dat
    --L1_inject injection_L1.dat
    
turn the signal model subtraction on:

    --signal

to stop script after signal subtraction for checking with some plots

    --signal_only
    
    