from gwpy.timeseries import TimeSeries
from gwpy.frequencyseries import FrequencySeries
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.signal import tukey
import argparse, os, logging
import datetime
import pycbc.waveform.waveform as wv


def parse_args():
    parser = argparse.ArgumentParser(description="InitGlitch.py: Script to find an optimal initialization point"
                                                 "for sine Gaussian glitch Fitting in LALInference")
    parser.add_argument("--data-files", help=".gwf files in order of psds or injections one for each detector", nargs='*', type=str)
    parser.add_argument("--inject-files", help=".dat files in order of data or psds one for each detector", nargs='*', type=str)
    parser.add_argument("--ifos", help='list string in (H, L, V, H1, V1, L1) for each detector used in order of data or psd files passed in', nargs="*", type=str)
    parser.add_argument("--channels", help='list of strings for channels wanted to use in order of data files passed', type=str, nargs='*')
    parser.add_argument("--H1_data_file", help=".gwf frame file path for H1 data", nargs='*', type=str)
    parser.add_argument("--L1_data_file", help=".gwf frame file path for L1 data", nargs='*', type=str)
    parser.add_argument("--H1_channel", help='channel name for H1 data found in .gwf file(s)', type=str)
    parser.add_argument("--L1_channel", help='channel name for L1 data found in .gwf file(s)', type=str)
    parser.add_argument("--start", "-s", default=None, help="start time (GPS)", type=float)
    parser.add_argument("--end", "-e", default=None, help="end time (GPS)", type=float)
    parser.add_argument("--debug", "-d", default=None, action='store_true')
    parser.add_argument("--nglitch", default=5, type=int, action='store', help='Number of Glitches to Initialize')
    parser.add_argument("--trigtime", help="trigger time of event", type=float)
    parser.add_argument("--seglen", default=8, type=float, help="Seglength of data to look at: Defaults to 8 seconds")
    parser.add_argument("--srate", default=4096, action='store', help="Sample rate (defaults to 4kHz)", type=float)
    parser.add_argument("--psds", help='.dat files to PSDS pass in multiple args for each detector PSD file', nargs='*',
                        type=str)
    parser.add_argument("--determine", default=False, action='store_true', help='Boolean flag to set a random seed or not')
    parser.add_argument("--opt_verbose", default=False, action='store_true', help='Boolean Flag to set optimizer display true or Not')
    parser.add_argument("--signal", default=False, action='store_true', help='Boolean flag to turn on signal subtraction')
    parser.add_argument("--signal_only", default=False, action='store_true', help='Boolean flag to stop script after signal '
                                                                                  'subtraction with plots to check result')
    parser.add_argument("--H1_inject", default=None, action='store', type=str, help='path to .dat file to injection data for H1')
    parser.add_argument("--L1_inject", default=None, action='store', type=str, help='path to .dat file to injection data for L1')

    return vars(parser.parse_args())


def setup_logger(outdir='./logs/', label=None, level='INFO'):
    if type(level) is str:
        try:
            level = getattr(logging, level.upper())
        except AttributeError:
            raise ValueError('log_level {} not understood'.format(level))
    else:
        level = int(level)

    logger = logging.getLogger(__name__)
    logger.propagate = False
    logger.setLevel(level)

    if any([type(h) == logging.StreamHandler for h in logger.handlers]) is False:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(name)s %(levelname)-8s: %(message)s', datefmt='%H:%M'))
        stream_handler.setLevel(level)
        logger.addHandler(stream_handler)

    if any([type(h) == logging.FileHandler for h in logger.handlers]) is False:
        if label:
            if outdir:
                if not os.path.exists(outdir):
                    os.mkdir(outdir)
            else:
                outdir = '.'
            log_file = '{}/{}.log'.format(outdir, label)
            file_handler = logging.FileHandler(log_file)
            file_handler.setFormatter(logging.Formatter(
                '%(asctime)s %(levelname)-8s: %(message)s', datefmt='%H:%M'))

            file_handler.setLevel(level)
            logger.addHandler(file_handler)

    for handler in logger.handlers:
        handler.setLevel(level)

    return logger


def main():
    starttime = datetime.datetime.now()
    args = parse_args()
    if args['debug']:
        logger = setup_logger(level='DEBUG')
        logger.debug("Logger Setup With level=DUBUG: ")
    else:
        logger = setup_logger()
        logger.info("Setup Logger with level=INFO: ")
    if args['start'] is None or args['end'] is None:
        start = args['trigtime'] - args['seglen']+2
        end = args['trigtime'] +2
        logger.debug("using trigtime and seglen to get start/end times: start = {}, end = {} ".format(start, end))
    elif args['start'] is not None and args['end'] is not None:
        start = args['start']
        end = args['end']
        logger.debug("using passed in start/end GPS times start = {} end = {}".format(start, end))
    else:
        logger.error("Input Start/End time or Trigtime/seglength Not Understood:")
        raise ValueError("Input start/end or trigtime/seglen not understood:")

    verb = args['opt_verbose']
    seglen = args['seglen']
    trigtime = args['trigtime']
    srate = args['srate']
    nglitch = args['nglitch']
    signal = args['signal']
    sig_only = args['signal_only']
    ifos = args['ifos']
    logger.info("Reading Time Series Data")

    ts = {}
    for i, ifo in enumerate(ifos):
        ts[ifo] = TimeSeries.read(args['data_files'][i], args['channels'][i], start=start, end=end)

    #H1_ts = TimeSeries.read(args['H1_data_file'], str(args['H1_channel']), start=start, end=end)
    #L1_ts = TimeSeries.read(args['L1_data_file'], str(args['L1_channel']), start=start, end=end)

    if args['determine']:
        logger.debug('Setting NumPy random Seed = 10')
        np.random.seed(10)

    logger.info("Windowing, resampling, and FFTing Data Time Series")
    for k in ts.keys():
        ts[k] = window(ts[k].resample(srate), seglen)
    #H1_ts = window(H1_ts.resample(srate), seglen)
    #L1_ts = window(L1_ts.resample(srate), seglen)


    logger.info("Fetching PSD data from File")
    freqs = {}
    psds = {}
    for ifo, file in enumerate(args['psds']):
        freqs[ifos[ifo]], psds[ifos[ifo]] = psd_f_from_file(file)

    #freqH1, psdH1 = psd_f_from_file(args['psds'][0])
    #freqL1, psdL1 = psd_f_from_file(args['psds'][1])

    if signal:
        if args['H1_inject'] is not None:
            H1_ts = inject_signal(args['H1_inject'], H1_ts, trigtime, srate, seglen)
        if args['L1_inject'] is not None:
            L1_ts = inject_signal(args['L1_inject'], L1_ts, trigtime, srate, seglen)

        logger.debug("Signal Model turned On: creating signal model")
        x0 = draw_signal_x0()
        logger.debug("Drew Starting point at mass1={}, mass2={}, dist={}".format(x0))
        signal_logl = build_signal_model(L1_ts.fft(), psdL1, trigtime, srate, seglen)
        logger.debug("Minimizing logl with signal model for L1")
        sol = minimize(signal_logl, x0, options={'disp': verb, 'maxiter': 15}, method='powell')
        logger.debug("Subtracting signal at optimized params: mass1={}, mass2={}, dist={}".format_map(sol.x))
        if sig_only:
            tmpL = L1_ts.copy()
            tmpH = H1_ts.copy()
        L1_ts = subtract_signal_model(L1_ts, sol.x, trigtime, srate, seglen)
        x0 = sol.x
        logger.debug("Using L1's params and starting at mass1={}, mass2={}, dist={}".format_map(x0))
        signal_logl = build_signal_model(H1_ts.fft(), psdH1, trigtime, srate, seglen)
        logger.debug("Minimizing logl with signal model for H1")
        sol = minimize(signal_logl, x0, options={'disp': verb, 'maxiter': 15}, method='powell')
        logger.debug("Subtracting signal at optimized params: mass1={}, mass2={}, dist={}".format_map(sol.x))
        H1_ts = subtracat_signal_model(H1_ts, sol.x, trigtime, srate, seglen)
        if sig_only:
            plot_sig_sub(L1_ts, tmpL, trigtime, srate, seglen, label='L1', outfile='./plots/L1_inj', show=True, zoom=True)
            plot_sig_sub(H1_ts, tmpH, trigtime, srate, seglen, label='H1', outfile='./plots/H1_inj', show=True, zoom=True)
            return
    nglitch=5


    L1_t, L1_f = max_t_f_from_ts(L1_ts)
    Q = check_glitch_locations(L1_ts,[(L1_t, L1_f)], trigtime, seglen, title='L1 Data', dataQ=5.6569,
                               outfile='./plots/L1_RAW_data.png', show=False)
    x0sL1 = []
    opt_glithsL1 = []
    solsL1 = []
    tfL1 = []
    for i in range(nglitch):
        L1_fs = L1_ts.fft()
        L1_t, L1_f = max_t_f_from_ts(L1_ts)
        tfL1.append((L1_t, L1_f))
        logger.info("Creating Q transforms of Original Data")
        logger.debug("Picking Random Starting Point")
        x0, bounds = draw_random_x0(seglen, srate)
        x0sL1.append(x0)
        logger.debug("Initial Optimixation Point in (Amp, Phi, Q) = {}".format(x0))
        logger.debug("Building optimization Function at time = {}, freq = {}".format(L1_t, L1_f))
        logl_func = build_fun(L1_t, L1_f, freqL1, L1_fs, psdL1, trigtime, srate, seglen, bounds)
        sol = minimize(logl_func, x0, options={'disp': verb, 'maxiter': 20}, method='powell')
        solsL1.append(sol.x)
        logger.debug("Building Glitch in Frequency Domain")
        FD = build_FD_glitch(sol.x[0], sol.x[1], sol.x[2], L1_t, L1_f, psdL1 ** (1./2.), seglen, srate, trigtime=trigtime)
        glitch = FrequencySeries(data = FD, frequencies=L1_fs.frequencies, epoch=trigtime)
        logger.debug("IFFT glitch to the Time Domain")
        glitch_timeseries = time_sens_ifft(glitch, L1_ts.times)
        opt_glithsL1.append(glitch_timeseries)
        L1_ts = L1_ts - glitch_timeseries

    cleaned_L1_timeseries = L1_ts

    _ = check_glitch_locations(cleaned_L1_timeseries, tfL1, trigtime, seglen, title='L1 Data - Glitchs', dataQ=Q,
                               outfile = './plots/L1_cleaned_{}_glitchs.png'.format(nglitch), show=False)

    H1_t, H1_f = max_t_f_from_ts(H1_ts)
    Q = check_glitch_locations(H1_ts, [(H1_t, H1_f)], trigtime, seglen, title='H1 Data', dataQ=5.6569,
                               outfile='./plots/H1_RAW_data.png', show=False)
    x0sH1 = []
    opt_glithsH1 = []
    solsH1 = []
    tfH1 = []
    for i in range(nglitch):
        H1_fs = H1_ts.fft()
        H1_t, H1_f = max_t_f_from_ts(H1_ts)
        tfH1.append((H1_t, H1_f))
        logger.info("Creating Q transforms of Original Data")
        logger.debug("Picking Random Starting Point")
        x0, bounds = draw_random_x0(seglen, srate)
        x0sH1.append(x0)
        logger.debug("Initial Optimixation Point in (Amp, Phi, Q) = {}".format(x0))
        logger.debug("Building optimization Function at time = {}, freq = {}".format(H1_t, H1_f))
        logl_func = build_fun(H1_t, H1_f, freqH1, H1_fs, psdH1, trigtime, srate, seglen, bounds)
        sol = minimize(logl_func, x0, options={'disp': verb, 'maxiter': 20}, method='powell')
        solsH1.append(sol.x)
        logger.debug("Building Glitch in Frequency Domain")
        FD = build_FD_glitch(sol.x[0], sol.x[1], sol.x[2], H1_t, H1_f, psdH1 ** (1. / 2.), seglen, srate,
                             trigtime=trigtime)
        glitch = FrequencySeries(data=FD, frequencies=H1_fs.frequencies, epoch=trigtime)
        logger.debug("IFFT glitch to the Time Domain")
        glitch_timeseries = time_sens_ifft(glitch, H1_ts.times)
        opt_glithsH1.append(glitch_timeseries)
        H1_ts = H1_ts - glitch_timeseries
        gllen = len(glitch_timeseries)
    #plot_glitch_add_noise(glitch_timeseries, H1_t, H1_f, seglen, trigtime, Q=5.6569)
    cleaned_H1_timeseries = H1_ts
    tslen = len(cleaned_L1_timeseries)
    _ = check_glitch_locations(cleaned_H1_timeseries, tfH1, trigtime, seglen, title='H1 Data - Glitchs', dataQ=Q,
                               outfile = './plots/H1_cleaned_{}_glitchs.png'.format(nglitch), show=False)

    logger.debug("Formating and saving data to directory: ./dataDump/")
    data_dict = pack_data(x0sH1, x0sL1, opt_glithsH1, opt_glithsL1, solsH1, solsL1, tfH1, tfL1,
                                   cleaned_H1_timeseries, cleaned_L1_timeseries)
    data_dump(data_dict, nglitch, gllen, tslen, trigtime, seglen)
    logger.info("Finished Script: Elapsed Time = {}".format(datetime.datetime.now()-starttime))
    return


def draw_signal_x0():
    mass1 = np.random.randint(4, 50)
    mass2 = np.random.randint(3, 50)
    dist = np.random.randint(1, 2000)
    return (mass1, mass2, dist)

def data_dump(data, nglitch, gllen, tslen, trigtime, seglen, sufix=''):
    A = np.empty((2, nglitch))
    p = np.empty((2, nglitch))
    Q = np.empty((2, nglitch))
    t = np.empty((2, nglitch))
    f = np.empty((2, nglitch))
    glitch_ts = np.empty((2, 5, gllen))
    cleaned_ts = np.empty((2, tslen))
    for i in range(2):
        ifo = 'H1' if i == 0 else 'L1'
        for j in range(nglitch):
            A[i,j] = data[ifo]['sol'][j][0]
            p[i,j] = data[ifo]['sol'][j][1]
            Q[i,j] = data[ifo]['sol'][j][2]
            t[i,j] = data[ifo]['tf'][j][0]-trigtime +seglen - 2
            f[i,j] = data[ifo]['tf'][j][1]
            glitch_ts[i, j, :] = data[ifo]['glitch'][j]
        cleaned_ts[i] = data[ifo]['CleanTS']
    np.savetxt('./dataDump/GLITCH_PARAMS_' + sufix + '.txt', np.column_stack([A.flatten(), p.flatten(), Q.flatten(), t.flatten(), f.flatten()]))
    np.savetxt('./dataDump/CleanedTimeSeries_' + sufix + '.txt', (cleaned_ts[0,:], cleaned_ts[1,:]))
    np.savetxt('./dataDump/GlitchTimeSeries_' + sufix + '.txt', (glitch_ts[0,0,:], glitch_ts[0,1,:], glitch_ts[0,2,:],
                                                                 glitch_ts[0,3,:], glitch_ts[0,4,:], glitch_ts[1,0,:],
                                                                 glitch_ts[1,1,:], glitch_ts[1,2,:], glitch_ts[1,3,:],
                                                                 glitch_ts[1,4,:]))

    return

def pack_data(x0H, x0L, glH, glL, solH, solL, tfH, tfL, cleanH, cleanL):
    H1_dict = {'x0': x0H, 'tf': tfH, 'sol': solH, 'glitch': glH, 'CleanTS': cleanH}
    L1_dict = {'x0': x0L, 'tf': tfL, 'sol': solL, 'glitch': glL, 'CleanTS': cleanL}
    return {'H1': H1_dict, 'L1': L1_dict}


def max_t_f_from_ts(ts):
    spec = ts.q_transform()  # fres=1/args['seglen'], tres=1/args['srate'])
    max_t_f = max_t_f_from_spec(spec)
    t, f = spec.times[max_t_f[0]].value, spec.frequencies[max_t_f[1]].value
    return t, f

def draw_random_x0(seglen, srate):
    dT = 1 / srate;
    n = srate * seglen
    Anorm = np.sqrt(2 * dT / n)
    Amin, Amax = 10 / Anorm, 1000 / Anorm
    phiMin, phiMax = 0, 2 * np.pi
    Qmin, Qmax = 3, 30
    Amp0 = Amin + (Amax - Amin) * np.random.rand()
    phi0 = phiMin + (phiMax - phiMin) * np.random.rand()
    Q0 = Qmin + (Qmax - Qmin) * np.random.rand()
    x0 = (Amp0, phi0, Q0)
    bounds = ((Amin, Amax), (phiMin, phiMax), (Qmin, Qmax))
    return x0, bounds

def build_fun(tmax, fmax, freqs, data, psds, trigtime, srate, seglen, bounds):
    def fun(x, t=tmax, f=fmax, data=data, psds=psds, trig=trigtime, seglen=seglen, srate=srate, bounds=bounds):
        for i, val in enumerate(x):
            min, max = bounds[i]
            if min < val < max:
                continue
            else:
                return np.inf
        A, p, Q = x
        asd = psds ** (1/2.)
        gl = build_FD_glitch(A, p, Q, t, f, asd, seglen, srate, trigtime=trig)
        return -logl(freqs, gl, data, psds)
    return fun

def window(ts, seglen, a=0.4):
    window = tukey(len(ts), alpha=a/seglen)
    return ts*window

def time_sens_ifft(fs, times):
    nout = (fs.size - 1) * 2
    dift = np.fft.irfft(fs.value * nout) / 2
    new = TimeSeries(dift, epoch=fs.epoch, times=times)
    return new

def build_FD_glitch(A, p, Q, t, f, asd, seglen, srate, trigtime=0):
    A = float(A)
    p = float(p)
    Q = float(Q)
    dT = 1 / srate
    dF = 1 / seglen
    if ~(0 <= t < seglen) and trigtime is not None:
        t = t-trigtime +seglen - 2
    n = srate * seglen
    Anorm = np.sqrt(2 * dT / n)
    Tobs = seglen
    fLow = 10.
    fHigh = srate/2.
    low = np.ceil(fLow / dF)
    high = np.floor(fHigh / dF)-1
    FD_glitch = np.zeros(int(srate*seglen/2)+1, dtype=np.complex)
    A = A*Anorm
    tau = Q/(2*np.pi) / f
    glitchLow = int(np.floor((f - 1. / tau) / dF))
    glitchHigh = int(np.ceil((f + 1 / tau) / dF))
    for i in range(glitchLow, glitchHigh):
        if i >= low and i <= high:
            amparg = (i * dF - f) * np.pi * tau
            phasearg = np.pi * i + p - 2 * np.pi * i * dF * (t-Tobs/2.)
            Ai = A * tau * 0.5 * np.sqrt(np.pi) * np.exp(-amparg * amparg) *asd[i] / np.sqrt(Tobs)
            real = Ai * np.cos(phasearg)
            imag = Ai * np.sin(phasearg)
            FD_glitch[i] = real + 1j * imag
    return FD_glitch


def psd_f_from_file(f):
    vals = np.genfromtxt(f)
    return vals[:,0], vals[:,1]


def logl(freqs, gl, d, psd, flow=10.):
    df = freqs[1]-freqs[0]
    sel = freqs > flow
    gg = np.sum(4.0*df*(gl.conj()[sel]*gl[sel]).real/psd[sel])
    gd = np.sum(4.0*df*(d.conj()[sel]*gl[sel]).real/psd[sel])
    logl = -0.5*(gg - 2.0*gd)
    return logl


def plot_spec(spec, tf, trig, seglen):
    tidx, fidx = tf
    f = spec.frequencies[fidx]
    t = spec.times[tidx]
    plt.figure(figsize=(15, 10))
    spec.plot()
    ax = plt.gca()
    ax.set_epoch(trig)
    plt.xlim(trig-seglen+2, trig+2)
    plt.yscale('log')
    plt.scatter(t, f, marker='x', s=25, color='r', zorder=10)
    plt.show()

def check_glitch_locations(clean_ts, tfs, trigtime, seglen, dataQ=None, title='', outfile=None, show=True):
    if dataQ is not None:
        qtr = clean_ts.q_transform(qrange=(dataQ, dataQ))
    else:
        qtr = clean_ts.q_transform()
    pl = qtr.plot()
    ax = pl.gca()
    ax.set_epoch(trigtime)
    ax.colorbar()
    plt.xlim(trigtime-seglen+2, trigtime+2)
    if len(tfs) > 1:
        title += '\n Subrtacted {} Glitches'.format(len(tfs))
    plt.title(title + '\n found q = {}'.format(qtr.q))
    for i in range(len(tfs)):
        t, f = tfs[i]
        ax.scatter(t, f, marker='x', s=25, zorder=10, label='Glitch_{}'.format(i))
    ax.legend()
    if outfile is not None:
        plt.savefig(outfile, dpi=350)
    if show:
        plt.show()
    return qtr.q

def build_signal_model(data, psds, trigtime, srate, seglen):
    def signal_func(x, data=data, psds=psds, trigtime=trigtime, srate=srate, seglen=seglen, flow=10.):
        fixed_params = {}
        fixeddparams['delta_f'] = 1./seglen
        fixeddparams['f_lower'] = flow
        fixeddparams['f_final'] = srate/2.
        fixeddparams['srate'] = srate
        fixeddparams['seglen'] = seglen
        fixeddparams['approximant'] = 'IMRPhenomD'
        fixeddparams['duration'] = 8.
        freqs = np.arange(flow, srate/2., 1./seglen)
        mass1, mass2, dist = x
        hp, hc = wv.get_fd_waveform(mass1=mass1, mass2=mass2, distance=dist, **fixed_params)
        fs = hp + 1j*hc
        return -logl(freqs, fs, data, psds)

def plot_glitch_add_noise(ts, oldmaxt, oldmaxf, seglen, trigtime, Q=11.5):
    glitch = ts.copy()
    noise = TimeSeries(np.random.randn(len(glitch))*0.1*max(glitch.value), times=glitch.times)
    glitch += noise
    qtr = glitch.q_transform(qrange=(Q, Q), whiten=False, fres=1/seglen)
    pl = qtr.plot()
    ax = pl.gca()
    ax.colorbar()
    ax.set_epoch(trigtime)
    newmaxt, newmaxf = np.unravel_index(np.argmax(qtr), qtr.shape)
    newmaxt = qtr.times[newmaxt]
    newmaxf = qtr.frequencies[newmaxf]
    ax.scatter(oldmaxt, oldmaxf, marker='x', color='r', label='old')
    ax.scatter(newmaxt, newmaxf, marker='x', color='darkorange', label='new')
    ax.legend()


def max_t_f_from_spec(spec):
    return np.unravel_index(np.argmax(spec), spec.shape)


def subtract_signal_model(ts, x, trigtime, srate, seglen):
    fixed_params = {}
    fixeddparams['delta_f'] = 1. / seglen
    fixeddparams['f_lower'] = flow
    fixeddparams['f_final'] = srate / 2.
    fixeddparams['srate'] = srate
    fixeddparams['seglen'] = seglen
    fixeddparams['approximant'] = 'IMRPhenomD'
    fixeddparams['duration'] = 8.
    freqs = np.arange(flow, srate / 2., 1. / seglen)
    mass1, mass2, dist = x
    hp, hc = wv.get_fd_waveform(mass1=mass1, mass2=mass2, distance=dist, **fixed_params)
    fs = hp + 1j * hc
    fs = ts.fft()-fs
    return fs.ifft()


def plot_sig_sub(diff, ts, trigtime, srate, seglen, label='', outfile=None, zoom=False, show=True):
    sig_qtr = ts.q_transform()
    sig_pl = sig_qtr.plot()
    ax1 = sig_pl.gca()
    ax1.set_epoch(trigtime)
    ax.colorbar()
    ax.set_title(label + 'Signal + Noise ')
    if zoom:
        ax.set_xlim(trigtime-1, trigtime+0.5)
    if outfile is not None:
        plt.savefig(outfile +'_signal', dpi=400)
    if show:
        plt.show()

    qtr = diff.q_transform()
    pl = qtr.plot()
    ax1 = pl.gca()
    ax1.set_epoch(trigtime)
    ax.colorbar()
    ax.set_title(label + 'Data - Signal Model')
    if zoom:
        ax.set_xlim(trigtime - 1, trigtime + 0.5)
    if outfile is not None:
        plt.savefig(outfile+'_sig_subtract', dpi=400)


def inject_signal(inj_file, ts, trig, srate, seglen):
    data = np.genfromtxt(inj_file)
    fs = ts.fft()
    freqs = np.arange(len(fs.frequencies))/seglen
    inj_fs = data[:,1]+1j*data[:,2]
    inj_fs = np.pad(inj_fs, (freqs.shape[0]-inj_fs.shape[0]-1, 1), 'constant', constant_values=(0,0))
    injection = FrequencySeries(inj_fs, frequencies=freqs, epoch=trig)
    signal_fs = fs + injection
    return time_sens_ifft(signal_fs, ts.times)


if __name__ == '__main__':
    __name__ = 'GlitchInitialization'
    main()